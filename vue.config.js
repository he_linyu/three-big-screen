
const { defineConfig } = require('@vue/cli-service')
const path = require('path')
const name = '数据大屏' // 网页标题
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    },
  },
})