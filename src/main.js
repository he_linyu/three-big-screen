/*
 * @Description: 
 * @Version: 2.10.8
 * @Author: 
 * @Date: 2023-10-20 15:44:56
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2023-10-31 11:25:52
 */
import Vue from 'vue'
import App from './App.vue'

import dataV from '@jiaminghi/data-view';
// 引入字体样式
import "@/assets/font/font.css"
Vue.use(dataV);

import dayjs from "dayjs"
// 引入echarts
import * as echarts from 'echarts';
Vue.prototype.$echarts = echarts;
Vue.prototype.$dayjs = dayjs;
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
