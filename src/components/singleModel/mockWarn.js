/*
 * @Description: 
 * @Version: 2.10.8
 * @Author: 
 * @Date: 2023-10-19 15:11:39
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2023-10-20 10:11:49
 */
export const mockWarnData = [
    {
        id: 'RackMount_Server028_Metal_0'
    },
    {
        id: 'RackMount_Server029_Metal_0'
    },
    {
        id: 'RackMount_Server030_Metal_0'
    },
    {
        id: 'RackMount_Server031_Metal_0'
    },
    {
        id: 'RackMount_Server032_Metal_0'
    },
    {
        id: 'RackMount_Server033_Metal_0'
    },
    {
        id: 'RackMount_Server034_Metal_0'
    },
    {
        id: 'RackMount_Server035_Metal_0'
    },
    {
        id: 'RackMount_Server036_Metal_0'
    },


    //第一排
    {
        id: 'Rectangle255_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle256_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle257_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle258_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle259_Black_Metal_Plate_0',
    },
    
    //第二排
    {
        id: 'Rectangle261_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle262_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle263_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle264_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle265_Black_Metal_Plate_0',
    },
    
    //第三排
    {
        id: 'Rectangle223_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle224_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle225_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle226_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle227_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle228_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle229_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle230_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle231_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle232_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle233_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle234_Black_Metal_Plate_0',
    },
    
    //第四排
    {
        id: 'Rectangle266_Black_Metal_Plate_0',
    },
    {
        id: 'Rectangle267_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle268_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle269_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle270_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle271_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle272_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle273_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle274_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle275_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle276_Black_Metal_Plate_0',
        
    },
    {
        id: 'Rectangle277_Black_Metal_Plate_0', 
    },
    //第五排
    {
        id: 'Rectangle235_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle236_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle237_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle238_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle239_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle240_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle241_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle242_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle243_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle244_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle245_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle246_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle247_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle248_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle249_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle250_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle251_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle252_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle253_Black_Metal_Plate_0', 
    },
    //第六排
    {
        id: 'Rectangle278_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle279_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle280_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle281_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle282_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle283_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle284_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle285_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle286_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle287_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle288_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle289_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle290_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle291_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle292_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle293_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle294_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle295_Black_Metal_Plate_0', 
    },
    {
        id: 'Rectangle296_Black_Metal_Plate_0', 
    },
]