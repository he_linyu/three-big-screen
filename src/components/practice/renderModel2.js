/* eslint-disable no-debugger */
/* eslint-disable no-unused-vars */
/* eslint-disable no-async-promise-executor */
import * as THREE from "three"; //导入整个 three.js核心库
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"; //导入控制器模块，轨道控制器
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"; //导入GLTF模块，模型解析器,根据文件格式来定
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
//import fragmentShader from "./fragment.glsl";
class renderModel {
  constructor(selector) {
    this.container = document.querySelector(selector);
    this.progress = 1.0;
    this.materialShader;
    this.cityGroup;
    this.circleMesh;
    this.circleShader;
    // 相机
    this.camera;
    // 场景
    this.scene;
    //渲染器
    this.renderer;
    // 控制器
    this.controls;
    // 模型
    this.model;
    // 标记模型
    this.maskModel;
    // 加载进度监听
    this.loadingManager = new THREE.LoadingManager();
    //文件加载器类型
    this.fileLoaderMap = {
      glb: new GLTFLoader(),
      fbx: new FBXLoader(this.loadingManager),
      gltf: new GLTFLoader(),
      obj: new OBJLoader(this.loadingManager),
    };
    //模型动画列表
    this.modelAnimation;
    //模型动画对象
    this.animationMixer;
    this.animationColock = new THREE.Clock();
    //动画帧
    this.animationFrame;
    // 轴动画帧
    this.rotationAnimationFrame;
    // 动画构造器
    this.animateClipAction = null;
    // 动画循环方式枚举
    this.loopMap = {
      LoopOnce: THREE.LoopOnce,
      LoopRepeat: THREE.LoopRepeat,
      LoopPingPong: THREE.LoopPingPong,
    };
    // 模型骨架
    this.skeletonHelper;
    // 网格辅助线
    this.gridHelper;
    // 坐标轴辅助线
    this.axesHelper;
    // 环境光
    this.ambientLight;
    //平行光
    this.directionalLight;
    // 平行光辅助线
    this.directionalLightHelper;
    // 点光源
    this.pointLight;
    //点光源辅助线
    this.pointLightHelper;
    //聚光灯
    this.spotLight;
    //聚光灯辅助线
    this.spotLightHelper;
    //模型平面
    this.planeGeometry;
    //模型材质列表
    this.modelMaterialList;
    // 效果合成器
    this.effectComposer;
    this.outlinePass;
    this.warnOutlinePass;
    // 动画渲染器
    this.renderAnimation;
    // 碰撞检测
    this.raycaster = new THREE.Raycaster();
    // 鼠标位置
    this.mouse = new THREE.Vector2();
    // 模型自带贴图
    this.modelTextureMap;
    // 辉光效果合成器
    this.glowComposer;
    // 辉光渲染器
    this.unrealBloomPass;
    // 需要辉光的材质
    this.glowMaterialList;
    this.materials = {};
    // 拖拽对象控制器
    this.dragControls;
    // 是否显示材质标签
    this.hoverMeshTag = true;
    // 窗口变化监听事件
    this.onWindowResizesListener;
    // 鼠标点击事件
    this.onMouseClickListener;
    // 鼠标按下
    this.onMouseDownListener;
    // 鼠标移动
    this.onMouseMoveListener;
    // 模型上传进度条回调函数
    this.modelProgressCallback = (e) => e;
  }
  init() {
    return new Promise(async (reslove, reject) => {
      //初始化场景
      this.initScene();
      //初始化渲染器
      this.initRender();
      //初始化相机
      this.initCamera();
      //初始化控制器，控制摄像头,控制器一定要在渲染器后
      this.initControls();
      // 创建辅助线
      this.createHelper();
      // 创建灯光
      this.createLight();

      const load = true;
      this.loadBoxRender();
      this.loadCircle();
      // 创建效果合成器
      //场景渲染
      this.sceneAnimation();
      reslove(load);
    });
  }

  loadBoxRender() {
    const geometry = new THREE.BoxGeometry(1, 4, 1); // 宽度、高度、深度
    const vertexShader = `
    varying vec2 vUv;
    varying vec3 vColor;
    uniform vec3 upColor;
    uniform vec3 downColor;
    uniform vec3 forceColor;

    void main() {
        // vColor = downColor;
        // if(position.y > 0.) {
        //     vColor = upColor;
        // }
        vColor = forceColor;
        vUv = uv;
        vec4 viewPosition = modelViewMatrix * vec4(position, 1.0);
        gl_Position = projectionMatrix * viewPosition;
    }
  `;
    const fragmentShader = `
    varying vec2 vUv;
    varying vec3 vColor;

    void main() {

        gl_FragColor = vec4(vColor,1.0);
    }
  `;
    // const material = new THREE.MeshLambertMaterial({
    //   color: 0x004444,
    //   transparent: true,
    //   opacity: 0.5,
    //   side: THREE.BackSide, // 设置side属性为THREE.BackSide
    // });
    const material = new THREE.ShaderMaterial({
      vertexShader,
      fragmentShader,
      uniforms: {
        upColor: { value: new THREE.Color("#f00") },
        downColor: { value: new THREE.Color("#030303") },
      },
    });
    const { random } = Math;
    const group = new THREE.Group();
    for (let i = 0; i < 500; i++) {
      const height = random() * 10;
      const cube = new THREE.Mesh(
        new THREE.BoxGeometry(random(), height, random()),
        material.clone()
      );
      cube.position.x = (0.5 - random()) * 50;
      cube.position.y = height / 2;
      cube.position.z = (0.5 - random()) * 50;
      group.add(cube);
    }

    const floor = new THREE.Mesh(
        new THREE.BoxGeometry(100, 0.1, 100),
        new THREE.MeshStandardMaterial({
            color: "#030303"
        })
    )
    this.cityGroup = group;
    this.scene.add(floor);
    this.scene.add(group);
    // cube.position.set(1, 1, 2);
    // console.log('本地矩阵1',cube.matrix.elements);
    // cube.position.set(1,3,4)
    // cube.updateMatrix();//更新矩阵，.matrix会变化
    // console.log('本地矩阵2',cube.matrix.elements);
    // const group = new THREE.Group();
    // group.add(cube);
    // cube.updateMatrixWorld();
    // console.log('本地矩阵',cube.matrix.elements);
    // console.log('世界矩阵',cube.matrixWorld.elements);
  }

  loadCircle() {
    const vertexShader = `
    varying vec2 vUv;

    void main() {
        vUv = uv;
        vec4 viewPosition = modelViewMatrix * vec4(position, 1.0);
        gl_Position = projectionMatrix * viewPosition;
    }
  `;
    const fragmentShader = `
    varying vec2 vUv;
    uniform float scale;
    uniform vec3 color1;
    uniform vec3 color2;

    void main() {
        float dis = distance(vUv,vec2(0.5,0.5));
        float opacity = smoothstep(0.4 * scale,0.5 * scale,dis);
        // vec3 color = vec3(1);

        vec3 disColor = color1 - color2;
        //a < b 1
        //a > b 0
        vec3 color = color2 + disColor * scale;
        opacity *= step(dis, 0.5 * scale);

        opacity -= (scale - 0.8) * 5. * step(0.8, scale);

        //glsl的if语句尽量少写，浪费性能
        // if(dis > (0.5 * scale)) {
        //     discard;
        // }
        gl_FragColor = vec4(color,opacity);
    }
  `;
    const geometry2 = new THREE.PlaneGeometry(100, 100);
    const material2 = new THREE.ShaderMaterial({
      uniforms: {
        scale: { value: 0 },
        color1: { value: new THREE.Color("#e2fb00") },
        color2: { value: new THREE.Color("#041cf3") },
        iTime: { value: 0 },
        iResolution: { value: new THREE.Vector2(1, 1) },
      },
      defines: {
        // 快速
        // FAST_DESCENT: true,
        // 黑白滤镜
        // BLACK_AND_WHITE: true,
      },
      // transparent: true,
      vertexShader,
      fragmentShader,
      transparent: true,
      side: THREE.DoubleSide,
    });
    const mesh = new THREE.Mesh(geometry2, material2);
    mesh.position.y = 0.5;
    mesh.rotateX(Math.PI / -2);
    this.circleShader = material2;
    this.circleMesh = mesh;
    this.scene.add(mesh);
  }

  // 更新场景
  sceneAnimation() {

    this.circleShader.uniforms.scale.value += 0.003;
    this.circleShader.uniforms.scale.value %= 1;
    const scale = this.circleShader.uniforms.scale.value;
    const far = scale * 50;
    const near = (scale - 0.1) * 50;
    this.cityGroup.children.forEach((box) => {
        const distance = box.position.distanceTo(this.circleMesh.position);
        if (distance > near && distance < far) {
            box.material.uniforms.forceColor = { value: new THREE.Color("#1ff") };
        } else {
            box.material.uniforms.forceColor = { value: new THREE.Color("#000") };
        }
    });
    this.renderer.render(this.scene, this.camera);
    this.renderAnimation = requestAnimationFrame(() => this.sceneAnimation());
    // this.controls.update();
  }

  // 创建渲染器
  initRender() {
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true }); //设置抗锯齿
    //设置屏幕像素比
    this.renderer.setPixelRatio(window.devicePixelRatio);
    //渲染的尺寸大小
    const { clientHeight, clientWidth } = this.container;
    this.renderer.setSize(clientWidth, clientHeight);
    //色调映射
    this.renderer.toneMapping = THREE.ReinhardToneMapping;
    this.renderer.outputColorSpace = THREE.SRGBColorSpace;
    //曝光
    this.renderer.toneMappingExposure = 3;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // this.renderer.setClearColor('#fff');
    this.container.appendChild(this.renderer.domElement);
  }

  // 创建相机
  initCamera() {
    const { clientHeight, clientWidth } = this.container;
    this.camera = new THREE.PerspectiveCamera(
      45,
      clientWidth / clientHeight,
      1,
      1000
    );
    this.camera.position.set(0, 100, 0);
  }

  // 创建场景
  initScene() {
    this.scene = new THREE.Scene();
    const texture = new THREE.TextureLoader().load(
      "/city/group/icon_20211207124700775_59418.jpg"
    );
    // texture.mapping = THREE.EquirectangularReflectionMapping;
    this.scene.background = texture;
    this.scene.environment = texture;
  }

  // 创建控制器
  initControls() {
    let controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.7;
    controls.enableZoom = true;
    controls.autoRotate = false;
    controls.autoRotateSpeed = 0.5;
    controls.enablePan = true;

    // let cameraStartPostion, cameraEndPostion;
    // controls.addEventListener("start", () => {
    //   cameraStartPostion = this.camera.position.clone();
    //   controls.isClickLock = false;
    // });

    // controls.addEventListener("end", () => {
    //   console.log(this.camera.position);
    //   console.log(this.controls.target);
    //   cameraEndPostion = this.camera.position;
    //   const startXYZ = Object.values(cameraStartPostion).reduce(function (
    //     prev,
    //     curr
    //   ) {
    //     return prev + curr;
    //   });
    //   const endXYZ = Object.values(cameraEndPostion).reduce(function (
    //     prev,
    //     curr
    //   ) {
    //     return prev + curr;
    //   });
    //   if (Math.abs(endXYZ - startXYZ) < 0.1) {
    //     controls.isClickLock = false;
    //   } else {
    //     controls.isClickLock = true;
    //   }
    //   cameraStartPostion = null;
    //   cameraEndPostion = null;
    // });
    this.controls = controls;
  }

  // 创建辅助线
  createHelper() {
    //网格辅助线
    this.gridHelper = new THREE.GridHelper(
      4,
      10,
      "rgb(193,193,193)",
      "rgb(193,193,193)"
    );
    this.gridHelper.position.set(0, -0.59, -0.1);
    this.gridHelper.visible = false;
    this.scene.add(this.gridHelper);
    // 坐标轴辅助线
    this.axesHelper = new THREE.AxesHelper(50);
    this.axesHelper.visible = true;
    this.scene.add(this.axesHelper);
    // 开启阴影
    this.renderer.shadowMap.enabled = true;
  }

  // 创建光源
  createLight() {
    //添加直线光
    let light1 = new THREE.DirectionalLight(0xffffff, 0.3);
    light1.position.set(0, 10, 10);
    let light2 = new THREE.DirectionalLight(0xffffff, 0.3);
    light2.position.set(0, 10, -10);
    let light3 = new THREE.DirectionalLight(0xffffff, 0.3);
    light3.position.set(10, 10, 10);
    this.scene.add(light1);
    this.scene.add(light2);
    this.scene.add(light3);
  }

  // 清除模型数据
  onClearModelData() {
    cancelAnimationFrame(this.rotationAnimationFrame);
    cancelAnimationFrame(this.renderAnimation);
    cancelAnimationFrame(this.animationFrame);
    this.container.removeEventListener("click", this.onMouseClickListener);
    this.container.removeEventListener("mousedown", this.onMouseDownListener);
    this.container.removeEventListener("mousemove", this.onMouseMoveListener);
    window.removeEventListener("resize", this.onWindowResizesListener);
    this.scene.traverse((v) => {
      if (v.type === "Mesh") {
        v.geometry.dispose();
        v.material.dispose();
      }
    });
    this.scene.clear();
    this.renderer.clear();
    this.container = null;
    // 相机
    this.camera = null;
    // 场景
    this.scene = null;
    //渲染器
    this.renderer = null;
    // 控制器
    this.controls = null;
    // 模型
    this.model = null;
    //文件加载器类型
    this.fileLoaderMap = null;
    //模型动画列表
    this.modelAnimation = null;
    //模型动画对象
    this.animationMixer = null;
    this.animationColock = null;
    //动画帧
    this.animationFrame = null;
    // 轴动画帧
    this.rotationAnimationFrame = null;
    // 动画构造器
    this.animateClipAction = null;
    // 动画循环方式枚举
    this.loopMap = null;
    // 模型骨架
    this.skeletonHelper = null;
    // 网格辅助线
    this.gridHelper = null;
    // 坐标轴辅助线
    this.axesHelper = null;
    // 环境光
    this.ambientLight = null;
    //平行光
    this.directionalLight = null;
    // 平行光辅助线
    this.directionalLightHelper = null;
    // 点光源
    this.pointLight = null;
    //点光源辅助线
    this.pointLightHelper = null;
    //聚光灯
    this.spotLight = null;
    //聚光灯辅助线
    this.spotLightHelper = null;
    //模型平面
    this.planeGeometry = null;
    //模型材质列表
    this.modelMaterialList = null;
    // 效果合成器
    this.effectComposer = null;
    this.outlinePass = null;
    this.warnOutlinePass = null;
    // 动画渲染器
    this.renderAnimation = null;
    // 碰撞检测
    this.raycaster == null;
    // 鼠标位置
    this.mouse = null;
    // 模型自带贴图
    this.modelTextureMap = null;
    // 辉光效果合成器
    this.glowComposer = null;
    // 辉光渲染器
    this.unrealBloomPass = null;
    // 需要辉光的材质
    this.glowMaterialList = null;
    this.materials = null;
    // 拖拽对象控制器
    this.dragControls = null;
    // 是否显示材质标签
    this.hoverMeshTag = null;
  }
}
export default renderModel;
