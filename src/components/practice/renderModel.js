/* eslint-disable no-debugger */
/* eslint-disable no-unused-vars */
/* eslint-disable no-async-promise-executor */
import * as THREE from "three"; //导入整个 three.js核心库
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"; //导入控制器模块，轨道控制器
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"; //导入GLTF模块，模型解析器,根据文件格式来定
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
//import fragmentShader from "./fragment.glsl";
class renderModel {
  constructor(selector) {
    this.container = document.querySelector(selector);
    this.progress = 1.0;
    this.materialShader;
    // 相机
    this.camera;
    // 场景
    this.scene;
    //渲染器
    this.renderer;
    // 控制器
    this.controls;
    // 模型
    this.model;
    // 标记模型
    this.maskModel;
    // 加载进度监听
    this.loadingManager = new THREE.LoadingManager();
    //文件加载器类型
    this.fileLoaderMap = {
      glb: new GLTFLoader(),
      fbx: new FBXLoader(this.loadingManager),
      gltf: new GLTFLoader(),
      obj: new OBJLoader(this.loadingManager),
    };
    //模型动画列表
    this.modelAnimation;
    //模型动画对象
    this.animationMixer;
    this.animationColock = new THREE.Clock();
    //动画帧
    this.animationFrame;
    // 轴动画帧
    this.rotationAnimationFrame;
    // 动画构造器
    this.animateClipAction = null;
    // 动画循环方式枚举
    this.loopMap = {
      LoopOnce: THREE.LoopOnce,
      LoopRepeat: THREE.LoopRepeat,
      LoopPingPong: THREE.LoopPingPong,
    };
    // 模型骨架
    this.skeletonHelper;
    // 网格辅助线
    this.gridHelper;
    // 坐标轴辅助线
    this.axesHelper;
    // 环境光
    this.ambientLight;
    //平行光
    this.directionalLight;
    // 平行光辅助线
    this.directionalLightHelper;
    // 点光源
    this.pointLight;
    //点光源辅助线
    this.pointLightHelper;
    //聚光灯
    this.spotLight;
    //聚光灯辅助线
    this.spotLightHelper;
    //模型平面
    this.planeGeometry;
    //模型材质列表
    this.modelMaterialList;
    // 效果合成器
    this.effectComposer;
    this.outlinePass;
    this.warnOutlinePass;
    // 动画渲染器
    this.renderAnimation;
    // 碰撞检测
    this.raycaster = new THREE.Raycaster();
    // 鼠标位置
    this.mouse = new THREE.Vector2();
    // 模型自带贴图
    this.modelTextureMap;
    // 辉光效果合成器
    this.glowComposer;
    // 辉光渲染器
    this.unrealBloomPass;
    // 需要辉光的材质
    this.glowMaterialList;
    this.materials = {};
    // 拖拽对象控制器
    this.dragControls;
    // 是否显示材质标签
    this.hoverMeshTag = true;
    // 窗口变化监听事件
    this.onWindowResizesListener;
    // 鼠标点击事件
    this.onMouseClickListener;
    // 鼠标按下
    this.onMouseDownListener;
    // 鼠标移动
    this.onMouseMoveListener;
    // 模型上传进度条回调函数
    this.modelProgressCallback = (e) => e;
  }
  init() {
    return new Promise(async (reslove, reject) => {
      //初始化场景
      this.initScene();
      //初始化渲染器
      this.initRender();
      //初始化相机
      this.initCamera();
      //初始化控制器，控制摄像头,控制器一定要在渲染器后
      this.initControls();
      // 创建辅助线
      this.createHelper();
      // 创建灯光
      this.createLight();

      const load = true;
      this.loadBoxRender();
      // 创建效果合成器
      //场景渲染
      this.sceneAnimation();
      reslove(load);
    });
  }

  loadBoxRender() {
    const geometry = new THREE.BoxGeometry(2, 3, 4); // 宽度、高度、深度

    const material = new THREE.MeshLambertMaterial({
      color: 0x004444,
      transparent: true,
      opacity: 0.5,
      side: THREE.BackSide, // 设置side属性为THREE.BackSide
    });

    const cube = new THREE.Mesh(geometry, material);
    // cube.position.set(1, 1, 2);
    // console.log('本地矩阵1',cube.matrix.elements);
    // cube.position.set(1,3,4)
    // cube.updateMatrix();//更新矩阵，.matrix会变化
    // console.log('本地矩阵2',cube.matrix.elements);
    const group = new THREE.Group();
    group.add(cube);
    group.position.set(2,3,4);
    cube.updateMatrixWorld();
    console.log('本地矩阵',cube.matrix.elements);
    console.log('世界矩阵',cube.matrixWorld.elements);
    this.scene.add(group);


    const vertexShader = `
    varying vec2 vUv;

    void main() {
        vUv = uv;
        vec4 viewPosition = modelViewMatrix * vec4(position, 1.0);
        gl_Position = projectionMatrix * viewPosition;
    }
  `;
  const fragmentShader = `
  uniform float iTime;
uniform vec2 iResolution;
varying vec2 vUv;
// #define FAST_DESCENT

// #define BLACK_AND_WHITE

#ifdef FAST_DESCENT
const vec3 cameraDir = normalize(vec3(-2.0, -1.0, -4.0));
const float cameraDist = 5.0;
const float speed = 3.0;
const float zoom = 2.5;

const vec3 windowColorA = vec3(0.0, 0.0, 1.5);
const vec3 windowColorB = vec3(0.5, 1.5, 2.0);

const float fogOffset = 2.5;
const float fogDensity = 0.6;
const vec3 fogColor = vec3(0.25, 0.0, 0.3);

const float lightHeight = 0.5;
const float lightSpeed = 0.2;
const vec3 lightColorA = vec3(0.6, 0.3, 0.1);
const vec3 lightColorB = vec3(0.8, 0.6, 0.4);

const vec3 signColorA = vec3(0.0, 0.0, 1.5);
const vec3 signColorB = vec3(3.0, 3.0, 3.0);
#else
const vec3 cameraDir = normalize(vec3(-2.0, -1.0, -2.0));
const float cameraDist = 9.0;
const float speed = 1.0;
const float zoom = 3.5;

const vec3 windowColorA = vec3(0.0, 0.0, 1.5);
const vec3 windowColorB = vec3(0.5, 1.5, 2.0);

const float fogOffset = 7.0;
const float fogDensity = 0.7;
const vec3 fogColor = vec3(0.25, 0.0, 0.3);

const float lightHeight = 0.0;
const float lightSpeed = 0.15;
const vec3 lightColorA = vec3(0.6, 0.3, 0.1);
const vec3 lightColorB = vec3(0.8, 0.6, 0.4);

const vec3 signColorA = vec3(0.0, 0.0, 1.5);
const vec3 signColorB = vec3(3.0, 3.0, 3.0);
#endif

const float tau = 6.283185;

float hash1(float p) {
    vec3 p3 = fract(p * vec3(5.3983, 5.4427, 6.9371));
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.x + p3.y) * p3.z);
}

float hash1(vec2 p2) {
    p2 = fract(p2 * vec2(5.3983, 5.4427));
    p2 += dot(p2.yx, p2.xy + vec2(21.5351, 14.3137));
    return fract(p2.x * p2.y * 95.4337);
}

float hash1(vec2 p2, float p) {
    vec3 p3 = fract(vec3(5.3983 * p2.x, 5.4427 * p2.y, 6.9371 * p));
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.x + p3.y) * p3.z);
}

vec2 hash2(vec2 p2, float p) {
    vec3 p3 = fract(vec3(5.3983 * p2.x, 5.4427 * p2.y, 6.9371 * p));
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.xx + p3.yz) * p3.zy);
}

vec3 hash3(vec2 p2) {
    vec3 p3 = fract(vec3(p2.xyx) * vec3(5.3983, 5.4427, 6.9371));
    p3 += dot(p3, p3.yxz + 19.19);
    return fract((p3.xxy + p3.yzz) * p3.zyx);
}

vec4 hash4(vec2 p2) {
    vec4 p4 = fract(p2.xyxy * vec4(5.3983, 5.4427, 6.9371, 7.1283));
    p4 += dot(p4, p4.yxwz + 19.19);
    return fract((p4.xxxy + p4.yyzz + p4.zwww) * p4.wzyx);
}

float noise(vec2 p) {
    vec2 i = floor(p);
    vec2 f = fract(p);
    vec2 u = f * f * (3.0 - 2.0 * f);
    return mix(mix(hash1(i + vec2(0.0, 0.0)), hash1(i + vec2(1.0, 0.0)), u.x), mix(hash1(i + vec2(0.0, 1.0)), hash1(i + vec2(1.0, 1.0)), u.x), u.y);
}

vec4 castRay(vec3 eye, vec3 ray, vec2 center) {
    vec2 block = floor(eye.xy);
    vec3 ri = 1.0 / ray;
    vec3 rs = sign(ray);
    vec3 side = 0.5 + 0.5 * rs;
    vec2 ris = ri.xy * rs.xy;
    vec2 dis = (block - eye.xy + 0.5 + rs.xy * 0.5) * ri.xy;

    for(int i = 0; i < 16; ++i) {
        float d = dot(block - center, cameraDir.xy);
        float height = 3.0 * hash1(block) - 1.0 + 1.5 * d - 0.1 * d * d;

        vec2 lo0 = vec2(block);
        vec2 loX = vec2(0.45, 0.45);
        vec2 hi0 = vec2(block + 0.55);
        vec2 hiX = vec2(0.45, 0.45);

        float dist = 500.0;
        float face = 0.0;

        {
            vec4 signHash = hash4(block);
            vec2 center = vec2(0.2, -0.4) + vec2(0.6, -0.8) * signHash.xy;
            float width = 0.06 + 0.1 * signHash.w;

            vec3 lo = vec3(center.x - width, 0.55, -100.0);
            vec3 hi = vec3(center.x + width, 0.99, center.y + width + height);

            float s = step(0.5, signHash.z);
            lo = vec3(block, 0.0) + mix(lo, lo.yxz, s);
            hi = vec3(block, 0.0) + mix(hi, hi.yxz, s);

            vec3 wall = mix(hi, lo, side);
            vec3 t = (wall - eye) * ri;

            vec3 dim = step(t.zxy, t) * step(t.yzx, t);
            float maxT = dot(dim, t);
            float maxFace = dim.x - dim.y;

            vec3 p = eye + maxT * ray;
            dim += step(lo, p) * step(p, hi);

            if(dim.x * dim.y * dim.z > 0.5) {
                dist = maxT;
                face = maxFace;
            }
        }

        for(int j = 0; j < 5; ++j) {
            float top = height - 0.4 * float(j);
            vec3 lo = vec3(lo0 + loX * hash2(block, float(j)), -100.0);
            vec3 hi = vec3(hi0 + hiX * hash2(block, float(j) + 0.5), top);

            vec3 wall = mix(hi, lo, side);
            vec3 t = (wall - eye) * ri;

            vec3 dim = step(t.zxy, t) * step(t.yzx, t);
            float maxT = dot(dim, t);
            float maxFace = dim.x - dim.y;

            vec3 p = eye + maxT * ray;
            dim += step(lo, p) * step(p, hi);

            if(dim.x * dim.y * dim.z > 0.5 && maxT < dist) {
                dist = maxT;
                face = maxFace;
            }
        }

        if(dist < 400.0) {
            return vec4(dist, height, face, 1.0);
        }

        float t = eye.z * ri.z;
        vec3 p = eye - t * ray;
        vec2 g = p.xy - block;

        vec2 dim = step(dis.xy, dis.yx);
        dis += dim * ris;
        block += dim * rs.xy;
    }

    return vec4(100.0, 0.0, 0.0, 1.0);
}

vec3 window(float z, vec2 pos, vec2 id) {
    float windowSize = 0.03 + 0.12 * hash1(id + 0.1);
    float windowProb = 0.3 + 0.8 * hash1(id + 0.2);
    float depth = z / windowSize;
    float level = floor(depth);
    vec3 colorA = mix(windowColorA, windowColorB, hash3(id));
    vec3 colorB = mix(windowColorA, windowColorB, hash3(id + 0.1));
    vec3 color = mix(colorA, colorB, hash1(id, level));
    color *= 0.3 + 0.7 * smoothstep(0.1, 0.5, noise(20.0 * pos + 100.0 * hash1(level)));
    color *= smoothstep(windowProb - 0.2, windowProb + 0.2, hash1(id, level + 0.1));
    return color * (0.5 - 0.5 * cos(tau * depth));
}

vec3 addLight(vec3 eye, vec3 ray, float res, float time, float height) {
    vec2 q = eye.xy + ((height - eye.z) / ray.z) * ray.xy;

    float row = floor(q.x + 0.5);
    time += hash1(row);
    float col = floor(0.125 * q.y - time);

    float pos = 0.4 + 0.4 * cos(time + tau * hash1(vec2(row, col)));
    vec3 lightPos = vec3(row, 8.0 * (col + time + pos), height);
    vec3 lightDir = vec3(0.0, 1.0, 0.0);

    // http://geomalgorithms.com/a07-_distance.html
    vec3 w = eye - lightPos;
    float a = dot(ray, ray);
    float b = dot(ray, lightDir);
    float c = dot(lightDir, lightDir);
    float d = dot(ray, w);
    float e = dot(lightDir, w);
    float D = a * c - b * b;
    float s = (b * e - c * d) / D;
    float t = (a * e - b * d) / D;

    t = max(t, 0.0);
    float dist = distance(eye + s * ray, lightPos + t * lightDir);

    float mask = smoothstep(res + 0.1, res, s);
    float light = min(1.0 / pow(200.0 * dist * dist / t + 20.0 * t * t, 0.8), 2.0);
    float fog = exp(-fogDensity * max(s - fogOffset, 0.0));
    vec3 color = mix(lightColorA, lightColorB, hash3(vec2(row, col)));
    return mask * light * fog * color;
}

vec3 addSign(vec3 color, vec3 pos, float side, vec2 id) {
    vec4 signHash = hash4(id);
    float s = step(0.5, signHash.z);
    if((s - 0.5) * side < 0.1)
        return color;

    vec2 center = vec2(0.2, -0.4) + vec2(0.6, -0.8) * signHash.xy;
    vec2 p = mix(pos.xz, pos.yz, s);
    float halfWidth = 0.04 + 0.06 * signHash.w;

    float charCount = floor(1.0 + 8.0 * hash1(id + 0.5));
    if(center.y - p.y > 2.0 * halfWidth * (charCount + 1.0)) {
        center.y -= 2.0 * halfWidth * (charCount + 1.5 + 5.0 * hash1(id + 0.6));
        charCount = floor(2.0 + 12.0 * hash1(id + 0.7));
        id += 0.05;
    }

    vec3 signColor = mix(signColorA, signColorB, hash3(id + 0.5));
    vec3 outlineColor = mix(signColorA, signColorB, hash3(id + 0.6));
    float flash = 6.0 - 24.0 * hash1(id + 0.8);
    flash *= step(3.0, flash);
    flash = smoothstep(0.1, 0.5, 0.5 + 0.5 * cos(flash * iTime));

    vec2 halfSize = vec2(halfWidth, halfWidth * charCount);
    center.y -= halfSize.y;
    float outline = length(max(abs(p - center) - halfSize, 0.0)) / halfWidth;
    color *= smoothstep(0.1, 0.4, outline);

    vec2 charPos = 0.5 * (p - center + halfSize) / halfWidth;
    vec2 charId = id + 0.05 + 0.1 * floor(charPos);
    float flicker = hash1(charId);
    flicker = step(0.93, flicker);
    flicker = 1.0 - flicker * step(0.96, hash1(charId, iTime));

    float char = -3.5 + 8.0 * noise(id + 6.0 * charPos);
    charPos = fract(charPos);
    char *= smoothstep(0.0, 0.4, charPos.x) * smoothstep(1.0, 0.6, charPos.x);
    char *= smoothstep(0.0, 0.4, charPos.y) * smoothstep(1.0, 0.6, charPos.y);
    color = mix(color, signColor, flash * flicker * step(outline, 0.01) * clamp(char, 0.0, 1.0));

    outline = smoothstep(0.0, 0.2, outline) * smoothstep(0.5, 0.3, outline);
    return mix(color, outlineColor, flash * outline);
}

void main() {
    vec2 center = -speed * iTime * cameraDir.xy;
    vec3 eye = vec3(center, 0.0) - cameraDist * cameraDir;

    vec3 forward = normalize(cameraDir);
    vec3 right = normalize(cross(forward, vec3(0.0, 0.0, 1.0)));
    vec3 up = cross(right, forward);
    vec2 xy = 2.0 * vUv - iResolution.xy;
    // vec2 xy = vUv;
    vec3 ray = normalize(xy.x * right + xy.y * up + zoom * forward * iResolution.y);

    vec4 res = castRay(eye, ray, center);
    vec3 p = eye + res.x * ray;

    vec2 block = floor(p.xy);
    vec3 color = window(p.z - res.y, p.xy, block);

    color = addSign(color, vec3(p.xy - block, p.z - res.y), res.z, block);
    color = mix(vec3(0.0), color, abs(res.z));

    float fog = exp(-fogDensity * max(res.x - fogOffset, 0.0));
    color = mix(fogColor, color, fog);

    float time = lightSpeed * iTime;
    color += addLight(eye.xyz, ray.xyz, res.x, time, lightHeight - 0.6);
    color += addLight(eye.yxz, ray.yxz, res.x, time, lightHeight - 0.4);
    color += addLight(vec3(-eye.xy, eye.z), vec3(-ray.xy, ray.z), res.x, time, lightHeight - 0.2);
    color += addLight(vec3(-eye.yx, eye.z), vec3(-ray.yx, ray.z), res.x, time, lightHeight);

#ifdef BLACK_AND_WHITE
    float c = clamp(dot(vec3(0.4, 0.3, 0.4), color), 0.0, 1.0);
    c = 1.0 - pow(1.0 - pow(c, 2.0), 4.0);
    color = vec3(c);
#endif

    gl_FragColor = vec4(color, 1.0);
}
  `
const geometry2 = new THREE.PlaneGeometry(10, 10);
const material2 = new THREE.ShaderMaterial({
  uniforms: {
    iTime: { value: 0 },
    iResolution: { value: new THREE.Vector2(1, 1) },
  },
  defines: {
      // 快速
      // FAST_DESCENT: true,
      // 黑白滤镜
      // BLACK_AND_WHITE: true,
  },
  // transparent: true,
  vertexShader,
  fragmentShader
});
const mesh = new THREE.Mesh(geometry2, material2);

mesh.onBeforeRender = () =>{
  material2.uniforms.iTime.value += 0.01; 
}
this.scene.add(mesh);
  }

  // 更新场景
  sceneAnimation() {
    // this.scene.rotation.y+=0.01;
    this.renderer.render(this.scene, this.camera);
    this.renderAnimation = requestAnimationFrame(() => this.sceneAnimation());
    // this.controls.update();
  }

  // 创建渲染器
  initRender() {
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true }); //设置抗锯齿
    //设置屏幕像素比
    this.renderer.setPixelRatio(window.devicePixelRatio);
    //渲染的尺寸大小
    const { clientHeight, clientWidth } = this.container;
    this.renderer.setSize(clientWidth, clientHeight);
    //色调映射
    this.renderer.toneMapping = THREE.ReinhardToneMapping;
    this.renderer.outputColorSpace = THREE.SRGBColorSpace;
    //曝光
    this.renderer.toneMappingExposure = 3;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // this.renderer.setClearColor('#fff');
    this.container.appendChild(this.renderer.domElement);
  }

  // 创建相机
  initCamera() {
    const { clientHeight, clientWidth } = this.container;
    this.camera = new THREE.PerspectiveCamera(
      45,
      clientWidth / clientHeight,
      1,
      1000
    );
    this.camera.position.set(0, 0, 20);
  }

  // 创建场景
  initScene() {
    this.scene = new THREE.Scene();
    const texture = new THREE.TextureLoader().load("/city/group/icon_20211207124700775_59418.jpg");
    // texture.mapping = THREE.EquirectangularReflectionMapping;
    this.scene.background = texture;
    this.scene.environment = texture;
  }

  // 创建控制器
  initControls() {
    let controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.7;
    controls.enableZoom = true;
    controls.autoRotate = false;
    controls.autoRotateSpeed = 0.5;
    controls.enablePan = true;

    // let cameraStartPostion, cameraEndPostion;
    // controls.addEventListener("start", () => {
    //   cameraStartPostion = this.camera.position.clone();
    //   controls.isClickLock = false;
    // });

    // controls.addEventListener("end", () => {
    //   console.log(this.camera.position);
    //   console.log(this.controls.target);
    //   cameraEndPostion = this.camera.position;
    //   const startXYZ = Object.values(cameraStartPostion).reduce(function (
    //     prev,
    //     curr
    //   ) {
    //     return prev + curr;
    //   });
    //   const endXYZ = Object.values(cameraEndPostion).reduce(function (
    //     prev,
    //     curr
    //   ) {
    //     return prev + curr;
    //   });
    //   if (Math.abs(endXYZ - startXYZ) < 0.1) {
    //     controls.isClickLock = false;
    //   } else {
    //     controls.isClickLock = true;
    //   }
    //   cameraStartPostion = null;
    //   cameraEndPostion = null;
    // });
    this.controls = controls;
  }

  // 创建辅助线
  createHelper() {
    //网格辅助线
    this.gridHelper = new THREE.GridHelper(
      4,
      10,
      "rgb(193,193,193)",
      "rgb(193,193,193)"
    );
    this.gridHelper.position.set(0, -0.59, -0.1);
    this.gridHelper.visible = false;
    this.scene.add(this.gridHelper);
    // 坐标轴辅助线
    this.axesHelper = new THREE.AxesHelper(50);
    this.axesHelper.visible = true;
    this.scene.add(this.axesHelper);
    // 开启阴影
    this.renderer.shadowMap.enabled = true;
  }

  // 创建光源
  createLight() {
    //添加直线光
    let light1 = new THREE.DirectionalLight(0xffffff, 0.3);
    light1.position.set(0, 10, 10);
    let light2 = new THREE.DirectionalLight(0xffffff, 0.3);
    light2.position.set(0, 10, -10);
    let light3 = new THREE.DirectionalLight(0xffffff, 0.3);
    light3.position.set(10, 10, 10);
    this.scene.add(light1);
    this.scene.add(light2);
    this.scene.add(light3);
  }



  // 清除模型数据
  onClearModelData() {
    cancelAnimationFrame(this.rotationAnimationFrame);
    cancelAnimationFrame(this.renderAnimation);
    cancelAnimationFrame(this.animationFrame);
    this.container.removeEventListener("click", this.onMouseClickListener);
    this.container.removeEventListener("mousedown", this.onMouseDownListener);
    this.container.removeEventListener("mousemove", this.onMouseMoveListener);
    window.removeEventListener("resize", this.onWindowResizesListener);
    this.scene.traverse((v) => {
      if (v.type === "Mesh") {
        v.geometry.dispose();
        v.material.dispose();
      }
    });
    this.scene.clear();
    this.renderer.clear();
    this.container = null;
    // 相机
    this.camera = null;
    // 场景
    this.scene = null;
    //渲染器
    this.renderer = null;
    // 控制器
    this.controls = null;
    // 模型
    this.model = null;
    //文件加载器类型
    this.fileLoaderMap = null;
    //模型动画列表
    this.modelAnimation = null;
    //模型动画对象
    this.animationMixer = null;
    this.animationColock = null;
    //动画帧
    this.animationFrame = null;
    // 轴动画帧
    this.rotationAnimationFrame = null;
    // 动画构造器
    this.animateClipAction = null;
    // 动画循环方式枚举
    this.loopMap = null;
    // 模型骨架
    this.skeletonHelper = null;
    // 网格辅助线
    this.gridHelper = null;
    // 坐标轴辅助线
    this.axesHelper = null;
    // 环境光
    this.ambientLight = null;
    //平行光
    this.directionalLight = null;
    // 平行光辅助线
    this.directionalLightHelper = null;
    // 点光源
    this.pointLight = null;
    //点光源辅助线
    this.pointLightHelper = null;
    //聚光灯
    this.spotLight = null;
    //聚光灯辅助线
    this.spotLightHelper = null;
    //模型平面
    this.planeGeometry = null;
    //模型材质列表
    this.modelMaterialList = null;
    // 效果合成器
    this.effectComposer = null;
    this.outlinePass = null;
    this.warnOutlinePass = null;
    // 动画渲染器
    this.renderAnimation = null;
    // 碰撞检测
    this.raycaster == null;
    // 鼠标位置
    this.mouse = null;
    // 模型自带贴图
    this.modelTextureMap = null;
    // 辉光效果合成器
    this.glowComposer = null;
    // 辉光渲染器
    this.unrealBloomPass = null;
    // 需要辉光的材质
    this.glowMaterialList = null;
    this.materials = null;
    // 拖拽对象控制器
    this.dragControls = null;
    // 是否显示材质标签
    this.hoverMeshTag = null;
  }
}
export default renderModel;
