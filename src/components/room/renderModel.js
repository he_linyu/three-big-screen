/* eslint-disable no-debugger */
/* eslint-disable no-unused-vars */
/* eslint-disable no-async-promise-executor */
import * as THREE from "three"; //导入整个 three.js核心库
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls"; //导入控制器模块，轨道控制器
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader"; //导入GLTF模块，模型解析器,根据文件格式来定
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer.js";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass.js";
import { OutlinePass } from "three/examples/jsm/postprocessing/OutlinePass.js";
import { ShaderPass } from "three/examples/jsm/postprocessing/ShaderPass.js";
import { FXAAShader } from "three/examples/jsm/shaders/FXAAShader.js";
import { UnrealBloomPass } from "three/addons/postprocessing/UnrealBloomPass.js";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader";
// import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader'
// import { GLTFExporter } from 'three/examples/jsm/exporters/GLTFExporter.js'
// import { OBJExporter } from 'three/examples/jsm/exporters/OBJExporter'
import { DragControls } from "three/examples/jsm/controls/DragControls";
import { vertexShader, fragmentShader, MODEL_DECOMPOSE } from "./constant.js";
class renderModel {
  constructor(selector) {
    this.container = document.querySelector(selector);
    // 相机
    this.camera;
    // 场景
    this.scene;
    //渲染器
    this.renderer;
    // 控制器
    this.controls;
    // 模型
    this.model;
    // 标记模型
    this.maskModel;
    // 加载进度监听
    this.loadingManager = new THREE.LoadingManager();
    //文件加载器类型
    this.fileLoaderMap = {
      glb: new GLTFLoader(),
      fbx: new FBXLoader(this.loadingManager),
      gltf: new GLTFLoader(),
      obj: new OBJLoader(this.loadingManager),
    };
    //模型动画列表
    this.modelAnimation;
    //模型动画对象
    this.animationMixer;
    this.animationColock = new THREE.Clock();
    //动画帧
    this.animationFrame;
    // 轴动画帧
    this.rotationAnimationFrame;
    // 动画构造器
    this.animateClipAction = null;
    // 动画循环方式枚举
    this.loopMap = {
      LoopOnce: THREE.LoopOnce,
      LoopRepeat: THREE.LoopRepeat,
      LoopPingPong: THREE.LoopPingPong,
    };
    // 模型骨架
    this.skeletonHelper;
    // 网格辅助线
    this.gridHelper;
    // 坐标轴辅助线
    this.axesHelper;
    // 环境光
    this.ambientLight;
    //平行光
    this.directionalLight;
    // 平行光辅助线
    this.directionalLightHelper;
    // 点光源
    this.pointLight;
    //点光源辅助线
    this.pointLightHelper;
    //聚光灯
    this.spotLight;
    //聚光灯辅助线
    this.spotLightHelper;
    //模型平面
    this.planeGeometry;
    //模型材质列表
    this.modelMaterialList;
    // 效果合成器
    this.effectComposer;
    this.outlinePass;
    this.warnOutlinePass;
    // 动画渲染器
    this.renderAnimation;
    // 碰撞检测
    this.raycaster = new THREE.Raycaster();
    // 鼠标位置
    this.mouse = new THREE.Vector2();
    // 模型自带贴图
    this.modelTextureMap;
    // 辉光效果合成器
    this.glowComposer;
    // 辉光渲染器
    this.unrealBloomPass;
    // 需要辉光的材质
    this.glowMaterialList;
    this.materials = {};
    // 拖拽对象控制器
    this.dragControls;
    // 是否显示材质标签
    this.hoverMeshTag = true;
    // 窗口变化监听事件
    this.onWindowResizesListener;
    // 鼠标点击事件
    this.onMouseClickListener;
    // 鼠标按下
    this.onMouseDownListener;
    // 鼠标移动
    this.onMouseMoveListener;
    // 模型上传进度条回调函数
    this.modelProgressCallback = (e) => e;
  }
  init() {
    return new Promise(async (reslove, reject) => {
      //初始化渲染器
      this.initRender();
      //初始化相机
      this.initCamera();
      //初始化场景
      this.initScene();
      //初始化控制器，控制摄像头,控制器一定要在渲染器后
      this.initControls();
      // 创建辅助线
      // this.createHelper();
      // 创建灯光
      this.createLight();

      this.addEvenListMouseListener();

      // 添加物体模型 TODO：初始化时需要默认一个
      const load = await this.setModel({
        filePath: "/room/room2.glb",
        fileType: "glb",
        decomposeName: "transformers_3",
        position: {
          x: 0,
          z: 0,
          y: 0,
        },
      });
      //this.loadBoxRender();
      // 创建效果合成器
      this.createEffectComposer();
      //场景渲染
      this.sceneAnimation();
      debugger;
      // this.onSetUnrealBloomPass();
      reslove(load);
    });
  }

  // 监听事件
  addEvenListMouseListener() {
    // //监听场景大小改变，跳转渲染尺寸
    this.onWindowResizesListener = this.onWindowResizes.bind(this)
    window.addEventListener("resize", this.onWindowResizesListener)
    // 鼠标点击
    this.onMouseClickListener = this.onMouseClickModel.bind(this);
    this.container.addEventListener("click", this.onMouseClickListener);
    // 鼠标按下
    // this.onMouseDownListener = this.onMouseDownModel.bind(this);
    // this.container.addEventListener("mousedown", this.onMouseDownListener);
    // 鼠标移动
    this.onMouseMoveListener = this.onMouseMoveModel.bind(this)
    this.container.addEventListener('mousemove', this.onMouseMoveListener)
  }
  // 遍历模型的材质，找到名称匹配的材质
  findAndMoveMaterialPosition(materialName) {
    this.model.traverse((object) => {
      if (object.isMesh && object.name === materialName) {
        this.warnOutlinePass.selectedObjects = [object];
      }
    });
  }
	// 监听窗口变化
	onWindowResizes() {
		if (!this.container) return false
		const { clientHeight, clientWidth } = this.container
		//调整屏幕大小
		this.camera.aspect = clientWidth / clientHeight //摄像机宽高比例
		this.camera.updateProjectionMatrix() //相机更新矩阵，将3d内容投射到2d面上转换
		this.renderer.setSize(clientWidth, clientHeight)
		this.effectComposer.setSize(clientWidth, clientHeight)
		this.glowComposer.setSize(clientWidth, clientHeight)
	}
  // 模型点击事件
  onMouseClickModel(event) {
    var { clientHeight, clientWidth, offsetLeft, offsetTop } = this.container;
    if(document.querySelector("#dv-full-screen-container")) {
      const str = document.querySelector("#dv-full-screen-container").style.transform;
  
      const regex = /\((.*?)\)/; // 匹配括号中的内容
      const match = str.match(regex);
  
      const number = parseFloat(match[1]);
      clientHeight = clientHeight * number;
      
      clientWidth = clientWidth * number;
    }

    console.log('宽：'+ clientWidth +' 高：'+ clientHeight)
    this.mouse.x = ((event.clientX - offsetLeft) / clientWidth) * 2 - 1;
    this.mouse.y = -((event.clientY - offsetTop) / clientHeight) * 2 + 1;
    this.raycaster.setFromCamera(this.mouse, this.camera);
    const intersects = this.raycaster
      .intersectObjects(this.scene.children)
      .filter((item) => item.object.isMesh);
    if (intersects.length > 0) {
      const intersectedObject = intersects[0].object;
      this.outlinePass.selectedObjects = [intersectedObject];
      
      if(intersectedObject.name.indexOf("Box") != -1 && intersectedObject.name.indexOf("Black_Metal_Plate") != -1){
        this.resetMaskModelPosition(intersectedObject)
      }
    } else {
      this.outlinePass.selectedObjects = [];
    }
  }

  resetMaskModelPosition(intersectedObject) {
      let intersectedObjectParent = intersectedObject.parent;
      if(intersectedObject.type != 'Group'){
        intersectedObjectParent = intersectedObject.parent;
        this.resetMaskModelPosition(intersectedObjectParent)
      }
      else{
        let {x,y,z}=intersectedObject.position;
        this.maskModel.position.set(x,3,z);
      }
  }

  // 鼠标选中材质
  onMouseDownModel() {
    const { clientHeight, clientWidth, offsetLeft, offsetTop } = this.container;
    this.mouse.x = ((event.clientX - offsetLeft) / clientWidth) * 2 - 1;
    this.mouse.y = -((event.clientY - offsetTop) / clientHeight) * 2 + 1;
    this.raycaster.setFromCamera(this.mouse, this.camera);
    const intersects = this.raycaster
      .intersectObjects(this.scene.children)
      .filter((item) => item.object.isMesh);
    if (intersects.length > 0) {
      const intersectedObject = intersects[0].object;
      // 设置当前选中的材质
      this.outlinePass.selectedObjects = [intersectedObject];
    } else {
      this.outlinePass.selectedObjects = [];
    }
  }


	// 鼠标移入模型材质
	onMouseMoveModel(event) {
		const { clientHeight, clientWidth, offsetLeft, offsetTop } = this.container
		this.mouse.x = ((event.clientX - offsetLeft) / clientWidth) * 2 - 1
		this.mouse.y = -((event.clientY - offsetTop) / clientHeight) * 2 + 1
		this.raycaster.setFromCamera(this.mouse, this.camera)
		const intersects = this.raycaster.intersectObjects(this.scene.children).filter(item => item.object.isMesh && this.glowMaterialList.includes(item.object.name))
		if (intersects.length > 0) {
			const meshTxt = document.getElementById("mesh-txt");
			// 判断是否开启显示材质标签
			if (this.hoverMeshTag) {
				// 设置材质标签位置
				const intersectedObject = intersects[0].object
				meshTxt.innerHTML = intersectedObject.name
				meshTxt.style.display = "block";
				meshTxt.style.top = event.clientY - offsetTop + 'px';
				meshTxt.style.left = event.clientX - offsetLeft + 20 + 'px';
			}
			document.body.style.cursor = 'pointer'

		} else {
			const meshTxt = document.getElementById("mesh-txt");
			document.body.style.cursor = '';
			meshTxt.style.display = "none";

		}
	}

  loadBoxRender() {
    const geometry = new THREE.BoxGeometry(1.4, 2.7, 1.4); // 宽度、高度、深度

    const material = new THREE.MeshLambertMaterial({
      color: 0x004444,
      transparent: true,
      opacity: 0.5,
      side: THREE.BackSide, // 设置side属性为THREE.BackSide
    });

    const cube = new THREE.Mesh(geometry, material);
    cube.position.set(0, 0.9, 0);

    const edges = new THREE.EdgesGeometry(geometry);
    const edgesMaterial = new THREE.LineBasicMaterial({
      color: 0x00ffff,
    });
    const line = new THREE.LineSegments(edges, edgesMaterial);
    line.position.set(0, 0.9, 0);

    this.scene.add(cube);
    this.scene.add(line);
  }

  // 创建渲染器
  initRender() {
    this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true }); //设置抗锯齿
    //设置屏幕像素比
    this.renderer.setPixelRatio(window.devicePixelRatio);
    //渲染的尺寸大小
    const { clientHeight, clientWidth } = this.container;
    this.renderer.setSize(clientWidth, clientHeight);
    //色调映射
    this.renderer.toneMapping = THREE.ReinhardToneMapping;
    this.renderer.outputColorSpace = THREE.SRGBColorSpace;
    //曝光
    this.renderer.toneMappingExposure = 3;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    // this.renderer.setClearColor('#fff');
    this.container.appendChild(this.renderer.domElement);
  }

  // 创建相机
  initCamera() {
    const { clientHeight, clientWidth } = this.container;
    this.camera = new THREE.PerspectiveCamera(
      45,
      clientWidth / clientHeight,
      1,
      1000
    );
  }

  // 创建场景
  initScene() {
    this.scene = new THREE.Scene();
    const texture = new THREE.TextureLoader().load("/vm/view-2.png");
    // texture.mapping = THREE.EquirectangularReflectionMapping;
    this.scene.background = texture;
    this.scene.environment = texture;
    //this.scene = new THREE.Scene();
    // const color = new THREE.Color("#fff");

    // // 将颜色值分配给背景和环境
    // this.scene.background = color;
    // this.scene.environment = color;
  }

  // 创建控制器
  initControls() {
    let controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.enableDamping = true;
    controls.dampingFactor = 0.7;
    controls.enableZoom = true;
    controls.autoRotate = false;
    controls.autoRotateSpeed = 0.5;
    controls.enablePan = true;

    let cameraStartPostion, cameraEndPostion;
    controls.addEventListener("start", () => {
      cameraStartPostion = this.camera.position.clone();
      controls.isClickLock = false;
    });

    controls.addEventListener("end", () => {
      console.log(this.camera.position);
      console.log(this.controls.target);
      cameraEndPostion = this.camera.position;
      const startXYZ = Object.values(cameraStartPostion).reduce(function (
        prev,
        curr
      ) {
        return prev + curr;
      });
      const endXYZ = Object.values(cameraEndPostion).reduce(function (
        prev,
        curr
      ) {
        return prev + curr;
      });
      if (Math.abs(endXYZ - startXYZ) < 0.1) {
        controls.isClickLock = false;
      } else {
        controls.isClickLock = true;
      }
      cameraStartPostion = null;
      cameraEndPostion = null;
    });
    this.controls = controls;
  }

  // 创建辅助线
  createHelper() {
    //网格辅助线
    // this.gridHelper = new THREE.GridHelper(
    //   4,
    //   10,
    //   "rgb(193,193,193)",
    //   "rgb(193,193,193)"
    // );
    // this.gridHelper.position.set(0, -0.59, -0.1);
    // this.gridHelper.visible = false;
    // this.scene.add(this.gridHelper);
    // // 坐标轴辅助线
    // this.axesHelper = new THREE.AxesHelper(2);
    // this.axesHelper.visible = true;
    // this.scene.add(this.axesHelper);
    // // 开启阴影
    // this.renderer.shadowMap.enabled = true;

    this.scene.add(new THREE.AxesHelper(100));
  }

  // 创建光源
  createLight() {
    //添加直线光
    // let light1 = new THREE.DirectionalLight(0xffffff, 0.3);
    // light1.position.set(0, 10, 10);
    // let light2 = new THREE.DirectionalLight(0xffffff, 0.3);
    // light2.position.set(0, 10, -10);
    // let light3 = new THREE.DirectionalLight(0xffffff, 0.3);
    // light3.position.set(10, 10, 10);
    // this.scene.add(light1);
    // this.scene.add(light2);
    // this.scene.add(light3);

    // 创建环境光
    // this.ambientLight = new THREE.AmbientLight("#fff", 0.8);
    // this.scene.add(this.ambientLight);

		// this.ambientLight.visible = true
		// this.ambientLight.intensity = 1
		// this.ambientLight.color.set("#fff")

    // // 创建平行光
    // this.directionalLight = new THREE.DirectionalLight("#fff", 1);
    // this.directionalLight.position.set(4, 4, 4);
    // this.directionalLight.castShadow = true;
    // this.directionalLight.visible = true;
    // this.scene.add(this.directionalLight);
    // // 创建平行光辅助线
    // this.directionalLightHelper = new THREE.DirectionalLightHelper(
    //   this.directionalLight,
    //   0.5
    // );
    // this.directionalLightHelper.visible = true;
    // this.scene.add(this.directionalLightHelper);

    // // 创建点光源
    // this.pointLight = new THREE.PointLight(0xffffff, 1, 100);
    // this.pointLight.position.set(4, 10, 4);
    // this.pointLight.visible = true;
    // this.scene.add(this.pointLight);
    // // 创建点光源辅助线
    // this.pointLightHelper = new THREE.PointLightHelper(this.pointLight, 0.5);
    // this.pointLightHelper.visible = true;
    // this.scene.add(this.pointLightHelper);

    // // 模型平面
    // const geometry = new THREE.PlaneGeometry(4, 4);
    // var groundMaterial = new THREE.MeshStandardMaterial({ color: "#939393" });
    // this.planeGeometry = new THREE.Mesh(geometry, groundMaterial);
    // this.planeGeometry.name = "planeGeometry";
    // this.planeGeometry.rotation.x = -Math.PI / 2;
    // this.planeGeometry.position.set(0, -0.5, 0);

    // // 让地面接收阴影
    // this.planeGeometry.receiveShadow = true;
    // this.planeGeometry.visible = false;
    // this.scene.add(this.planeGeometry);
  }

  // 创建效果合成器
  createEffectComposer() {
    const { clientHeight, clientWidth } = this.container;
    this.effectComposer = new EffectComposer(this.renderer);
    const renderPass = new RenderPass(this.scene, this.camera);
    this.effectComposer.addPass(renderPass);
    this.outlinePass = new OutlinePass(
      new THREE.Vector2(clientWidth, clientHeight),
      this.scene,
      this.camera
    );
    this.outlinePass.visibleEdgeColor = new THREE.Color("#FF8C00"); // 可见边缘的颜色
    this.outlinePass.hiddenEdgeColor = new THREE.Color("#8a90f3"); // 不可见边缘的颜色
    this.outlinePass.edgeGlow = 2.0; // 发光强度
    //this.outlinePass.usePatternTexture = false // 是否使用纹理图案
    this.outlinePass.edgeThickness = 1; // 边缘浓度
    this.outlinePass.edgeStrength = 4; // 边缘的强度，值越高边框范围越大
    this.outlinePass.pulsePeriod = 0; // 闪烁频率，值越大频率越低
    this.effectComposer.addPass(this.outlinePass);

    this.warnOutlinePass = new OutlinePass(
      new THREE.Vector2(clientWidth, clientHeight),
      this.scene,
      this.camera
    );
    this.warnOutlinePass.visibleEdgeColor = new THREE.Color("#ff4545"); // 可见边缘的颜色
    this.warnOutlinePass.hiddenEdgeColor = new THREE.Color("#c05757"); // 不可见边缘的颜色
    this.warnOutlinePass.edgeGlow = 2.0; // 发光强度
    //this.outlinePass.usePatternTexture = false // 是否使用纹理图案
    this.warnOutlinePass.edgeThickness = 1; // 边缘浓度
    this.warnOutlinePass.edgeStrength = 4; // 边缘的强度，值越高边框范围越大
    this.warnOutlinePass.pulsePeriod = 2; // 闪烁频率，值越大频率越低
    this.effectComposer.addPass(this.warnOutlinePass);

    let effectFXAA = new ShaderPass(FXAAShader);
    const pixelRatio = this.renderer.getPixelRatio();
    effectFXAA.uniforms.resolution.value.set(
      1 / (clientWidth * pixelRatio),
      1 / (clientHeight * pixelRatio)
    );
    effectFXAA.renderToScreen = true;
    effectFXAA.needsSwap = true;
    this.effectComposer.addPass(effectFXAA);

    //创建辉光效果
    this.unrealBloomPass = new UnrealBloomPass(
      new THREE.Vector2(clientWidth, clientHeight),
      0,
      0,
      0
    );
    this.unrealBloomPass.threshold = 0;
    this.unrealBloomPass.strength = 0;
    this.unrealBloomPass.radius = 0;
    this.unrealBloomPass.renderToScreen = false;
    // 辉光合成器
    this.glowComposer = new EffectComposer(this.renderer);
    this.glowComposer.renderToScreen = false;
    this.glowComposer.addPass(new RenderPass(this.scene, this.camera));
    this.glowComposer.addPass(this.unrealBloomPass);
    // 着色器
    let shaderPass = new ShaderPass(
      new THREE.ShaderMaterial({
        uniforms: {
          baseTexture: { value: null },
          bloomTexture: { value: this.glowComposer.renderTarget2.texture },
          tDiffuse: {
            value: null,
          },
        },
        vertexShader,
        fragmentShader,
        defines: {},
      }),
      "baseTexture"
    );

    shaderPass.renderToScreen = true;
    shaderPass.needsSwap = true;
    this.effectComposer.addPass(shaderPass);
  }

  async loadMultipleModels() {
    return new Promise(async (resolve, reject) => {
      const models = [];
      let position = {
        x: 0,
        z: 0,
        y: 0,
      };

      for (let i = 0; i < 5; i++) {
        const model = await this.setModel({
          filePath: "/vm/server_racking_system.glb",
          fileType: "glb",
          decomposeName: "transformers_3",
          position: position,
        });
  
        models.push(model);

        // 每五个为一行，超过五个后重置 x 和增加 y
        if ((i + 1) % 5 === 0) {
          position.x = 0;
          position.z += 3;
        } else {
          position.x += 1.1;
        }
      }
      resolve(true);
    });
  }

  // 加载模型
  setModel({ filePath, fileType, scale, map, position, decomposeName }) {
    return new Promise((resolve, reject) => {
      const loader = this.fileLoaderMap[fileType];
      if (["glb", "gltf"].includes(fileType)) {
        const dracoLoader = new DRACOLoader();
        dracoLoader.setDecoderPath("./threeFile/gltf/");
        loader.setDRACOLoader(dracoLoader);
      }
      loader.load(
        filePath,
        (result) => {
          switch (fileType) {
            case "glb":
              this.model = result.scene;
              this.model.traverse((child) => {
                if(child.type === "PointLight"){
                  child.power = 0.5;
                }
                if (child.name.indexOf("qiangti02") != -1) {
                  // 修改名为"墙体"的子对象的材质
                  child.material = new THREE.MeshStandardMaterial({ color: 0xffffff, receiveShadow: true });
                }
              });
              this.skeletonHelper = new THREE.SkeletonHelper(result.scene);
              this.modelAnimation = result.animations || [];
              break;
            case "fbx":
              this.model = result;
              break;
            case "gltf":
              this.model = result.scene;
              this.skeletonHelper = new THREE.SkeletonHelper(result.scene);
              this.modelAnimation = result.animations || [];
              break;
            case "obj":
              this.model = result;
              this.skeletonHelper = new THREE.SkeletonHelper(result);
              this.modelAnimation = result.animations || [];
              break;
            default:
              break;
          }
          this.model.decomposeName = decomposeName;
          this.getModelMeaterialList(map);
          this.setModelPositionSize();
          //设置模型大小
          if (scale) {
            this.model.scale.set(scale, scale, scale);
          }
          //设置模型位置
          //   this.model.position.set(0, 0, 0);
          if (position) {
            const { x, y, z } = position;
            this.model.position.set(x, y, z);
          }



          this.skeletonHelper.visible = false;
          this.scene.add(this.skeletonHelper);
          // 需要辉光的材质
          this.glowMaterialList = this.modelMaterialList.map((v) => v.name);
          this.scene.add(this.model);

          resolve(true);
        },
        (xhr) => {
          this.modelProgressCallback(xhr.loaded);
        },
        (err) => {
          console.log(err);
          reject();
        }
      );
    });
  }

  /**
   * @describe 材质模块方法
   * @function getModelMeaterialList 获取当前模型材质
   * @function setModelPositionSize 设置模型定位缩放大小
   * @function getModelMaps 获取模型自带贴图
   * @function onSetModelMaterial 设置材质属性（网格,透明度，颜色，深度写入）
   * @function onSetModelMap 设置模型贴图（模型自带）
   * @function onSetSystemModelMap 设置模型贴图（系统贴图）
   * @function onChangeModelMeaterial 选择材质
   * @function onMouseDownModel 鼠标选中材质
   * @function onGetEditMeshList 获取最新材质信息列表
   */
  // 获取当前模型材质
  getModelMeaterialList(map) {
    const isMap = map ? true : false;
    this.modelMaterialList = [];
    this.modelTextureMap = [];
    let i = 0;
    this.model.traverse((v) => {
      const { uuid } = v;
      if (v.isMesh) {
        v.castShadow = true;
        v.frustumCulled = false;
        i++;
        if (v.material) {
          const materials = Array.isArray(v.material)
            ? v.material
            : [v.material];
          const { name, color, map } = v.material;
          // 统一将模型材质 设置为 MeshLambertMaterial 类型
          v.material = new THREE.MeshStandardMaterial({
            map,
            transparent: true,
            color,
            name,
          });
          this.modelMaterialList.push(v);
          // 获取模型自动材质贴图
          const { url, mapId } = this.getModelMaps(materials, uuid);
          const mesh = {
            meshName: v.name,
            material: v.material,
            url,
            mapId: mapId + "_" + i,
          };
          // 获取当前模型材质
          v.mapId = mapId + "_" + i;
          this.modelTextureMap.push(mesh);
        }
        // 部分模型本身没有贴图需 要单独处理
        if (v.material && isMap) {
          const mapTexture = new THREE.TextureLoader().load(map);
          const { color, name } = v.material;
          v.material = new THREE.MeshStandardMaterial({
            map: mapTexture,
            name,
            transparent: true,
            color,
          });
          v.mapId = uuid + "_" + i;
          this.modelTextureMap = [
            {
              meshName: v.name,
              material: v.material,
              url: map,
              mapId: uuid + "_" + i,
            },
          ];
        }
      }
    });
  }
  // 设置模型定位缩放大小
  setModelPositionSize() {
    this.model.updateMatrixWorld();
    const box = new THREE.Box3().setFromObject(this.model);
    const size = box.getSize(new THREE.Vector3());
    const center = box.getCenter(new THREE.Vector3());
    // 计算缩放比例
    const maxSize = Math.max(size.x, size.y, size.z);
    const targetSize = 2.5; // 目标大小
    const scale = targetSize / (maxSize > 1 ? maxSize : 0.5);
    this.model.scale.set(scale, scale, scale);
    // this.model.position.sub(center.multiplyScalar(scale));
    // 设置控制器最小缩放值
    this.controls.maxDistance = size.length() * 10;
    // 设置相机位置
    this.camera.position.set(0, 2, 6);
    // 设置相机坐标系
    this.camera.lookAt(0, 0, 0);
    this.camera.updateProjectionMatrix();
  }

  // 获取模型自带贴图
  getModelMaps(materials, uuid) {
    let textureMap = {};
    materials.forEach((texture) => {
      if (texture.map && texture.map.image) {
        const canvas = document.createElement("canvas");
        const { width, height } = texture.map.image;
        canvas.width = width;
        canvas.height = height;
        const context = canvas.getContext("2d");
        context.drawImage(texture.map.image, 0, 0);
        textureMap = {
          url: canvas.toDataURL("image/png"),
          mapId: texture.uuid,
        };
      }
    });
    return textureMap;
  }

  // 设置辉光效果
  onSetUnrealBloomPass() {
    this.unrealBloomPass.threshold = 0.05;
    this.unrealBloomPass.strength = 1;
    this.unrealBloomPass.radius = 1;
    this.renderer.toneMappingExposure = 3;
  }

  // 更新场景
  sceneAnimation() {
    //this.renderer.render(this.scene, this.camera);

    this.renderAnimation = requestAnimationFrame(() => this.sceneAnimation());
    this.controls.update();
    //将不需要处理辉光的材质进行存储备份
    this.scene.traverse((v) => {
      if (v instanceof THREE.Scene) {
        this.materials.scene = v.background;
        v.background = null;
      }
      if (!this.glowMaterialList.includes(v.name) && v.isMesh) {
        this.materials[v.uuid] = v.material;
        v.material = new THREE.MeshBasicMaterial({ color: "black" });
      }
    });
    this.glowComposer.render();
    // 在辉光渲染器执行完之后在恢复材质原效果
    this.scene.traverse((v) => {
      if (this.materials[v.uuid]) {
        v.material = this.materials[v.uuid];
        delete this.materials[v.uuid];
      }
      if (v instanceof THREE.Scene) {
        v.background = this.materials.scene;
        delete this.materials.scene;
      }
    });
    this.effectComposer.render();
  }

  // 设置模型轴动画
  onSetRotation(config) {
    const { rotationVisible, rotationType, rotationSpeed } = config;
    if (rotationVisible) {
      cancelAnimationFrame(this.rotationAnimationFrame);
      this.rotationAnimationFun(rotationType, rotationSpeed);
    } else {
      cancelAnimationFrame(this.rotationAnimationFrame);
      this.maskModel.rotation.set(0, 0, 0);
    }
  }

  // 设置轴动画类型
  onSetRotationType(config) {
    const { rotationType, rotationSpeed } = config;
    this.maskModel.rotation.set(0, 0, 0);
    cancelAnimationFrame(this.rotationAnimationFrame);
    this.rotationAnimationFun(rotationType, rotationSpeed);
  }

  // 轴动画帧
  rotationAnimationFun(rotationType, rotationSpeed) {
    this.rotationAnimationFrame = requestAnimationFrame(() =>
      this.rotationAnimationFun(rotationType, rotationSpeed)
    );
    this.maskModel.rotation[rotationType] += rotationSpeed / 50;
  }

  // 清除模型数据
  onClearModelData() {
    cancelAnimationFrame(this.rotationAnimationFrame);
    cancelAnimationFrame(this.renderAnimation);
    cancelAnimationFrame(this.animationFrame);
    this.container.removeEventListener("click", this.onMouseClickListener);
    this.container.removeEventListener("mousedown", this.onMouseDownListener);
    this.container.removeEventListener("mousemove", this.onMouseMoveListener);
    window.removeEventListener("resize", this.onWindowResizesListener);
    this.scene.traverse((v) => {
      if (v.type === "Mesh") {
        v.geometry.dispose();
        v.material.dispose();
      }
    });
    this.scene.clear();
    this.renderer.clear();
    this.container = null;
    // 相机
    this.camera = null;
    // 场景
    this.scene = null;
    //渲染器
    this.renderer = null;
    // 控制器
    this.controls = null;
    // 模型
    this.model = null;
    //文件加载器类型
    this.fileLoaderMap = null;
    //模型动画列表
    this.modelAnimation = null;
    //模型动画对象
    this.animationMixer = null;
    this.animationColock = null;
    //动画帧
    this.animationFrame = null;
    // 轴动画帧
    this.rotationAnimationFrame = null;
    // 动画构造器
    this.animateClipAction = null;
    // 动画循环方式枚举
    this.loopMap = null;
    // 模型骨架
    this.skeletonHelper = null;
    // 网格辅助线
    this.gridHelper = null;
    // 坐标轴辅助线
    this.axesHelper = null;
    // 环境光
    this.ambientLight = null;
    //平行光
    this.directionalLight = null;
    // 平行光辅助线
    this.directionalLightHelper = null;
    // 点光源
    this.pointLight = null;
    //点光源辅助线
    this.pointLightHelper = null;
    //聚光灯
    this.spotLight = null;
    //聚光灯辅助线
    this.spotLightHelper = null;
    //模型平面
    this.planeGeometry = null;
    //模型材质列表
    this.modelMaterialList = null;
    // 效果合成器
    this.effectComposer = null;
    this.outlinePass = null;
    this.warnOutlinePass = null;
    // 动画渲染器
    this.renderAnimation = null;
    // 碰撞检测
    this.raycaster == null;
    // 鼠标位置
    this.mouse = null;
    // 模型自带贴图
    this.modelTextureMap = null;
    // 辉光效果合成器
    this.glowComposer = null;
    // 辉光渲染器
    this.unrealBloomPass = null;
    // 需要辉光的材质
    this.glowMaterialList = null;
    this.materials = null;
    // 拖拽对象控制器
    this.dragControls = null;
    // 是否显示材质标签
    this.hoverMeshTag = null;
  }
}
export default renderModel;
